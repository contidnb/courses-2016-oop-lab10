package it.unibo.oop.lab.reactivegui01;

/**
 * TestMatrix class for first reactive GUI.
 *
 */
public final class Test {

    private Test() {

    }

    /**
     * Analizzare il comportamento di CGUI e testarne il funzionamento eseguendo
     * questo test.
     * 
     * @param args
     *            possible command line arguments (not used)
     * 
     */
    public static void main(final String... args) {
        new ConcurrentGUI();
    }
}
